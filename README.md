# Create container

Create local lxc container and install ansible prerequisites inside.

`bash -c "$(wget https://gitlab.com/pesol/ansible-prerequisites-installer/-/blob/main/create_container.sh?inline=false -O -)" '' --name=container_name --dist=debian --release=bullseye`

# Ansible Prerequisites installer

Install sudo, nano, openssh-server and python3.

Create odoo and pesol users.

Make pesol sudo without passwords.

Add public key.

`bash -c "$(wget https://gitlab.com/pesol/ansible-prerequisites-installer/-/raw/main/install.sh?inline=false -O -)"`

Without parameters:

- odoo_passwd=odoo
- pesol_passwd=pesol
- ssh_port=22

To execute with parameters:

`bash -c "$(wget https://gitlab.com/pesol/ansible-prerequisites-installer/-/raw/main/install.sh?inline=false -O -)" '' --odoo_passwd=odoo_passwd --pesol_passwd=pesol_passwd --ssh_port=2222`
