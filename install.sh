#!/bin/bash

# ansible_pre_requisites.sh --odoo_passwd=xxx --pesol_passwd=xxx --ssh_port=xxx 

while [ $# -gt 0 ]; do
  case "$1" in
    --odoo_passwd=*)
      odoo_passwd="${1#*=}"
      ;;
    --pesol_passwd=*)
      pesol_passwd="${1#*=}"
      ;;
    --ssh_port=*)
      ssh_port="${1#*=}"
      ;;
    *)
      printf "***************************\n"
      printf "* Error: Invalid argument.*\n"
      printf "***************************\n"
      exit 1
  esac
  shift
done

odoo_passwd="${odoo_passwd:-odoo}"
pesol_passwd="${pesol_passwd:-pesol}"
ssh_port="${ssh_port:-22}"

# wait to network started
while [ "$(hostname -I)" = "" ]; do
  echo -e "\e[1A\e[KNo network: $(date)"
  sleep 1
done

# TODO
apt-get install -y --fix-missing
apt-get update -y --fix-missing
apt-get install openssh-server -y
apt-get install sudo -y
apt-get install nano -y 
sudo apt install python3 -y

# adduser odoo
odoo_username=odoo
adduser --gecos "" --disabled-password $odoo_username
chpasswd <<<"$odoo_username:$odoo_passwd"
# adduser pesol
pesol_username=pesol
adduser --gecos "" --disabled-password $pesol_username
chpasswd <<<"$pesol_username:$pesol_passwd"

# Grant SUDO access
echo 'pesol     ALL=(ALL) NOPASSWD:ALL'>>/etc/sudoers

## Allow connect without password
mkdir /home/pesol/.ssh
touch /home/pesol/.ssh/authorized_keys
# --------------------------------------------------------------
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCvvn3I/FOYdnhYVU/6F3PuBqss4QOaBMLrxiRA4E+z2VaN8omizeIl5y1dFnUcwnNylJfjPI2o+s1ew8SaeNDf3+rTsKTzY2wG/EkW+gmzaMw2nRh5fsd1cu652Yss/CNvVQmUT6Bvf5+7K57+JH+ecmNmzyCijFrvQEFliZDrr9DTzX9sH9YW+m+zHIDOOOCYiAKBGGma37FOnDSLXXDDUXy2MjXMZFfltwnlxwk0hNAKgP+IrnX+3FLjqGaNRfHjThz9Ytu5ESD/gc/GWdO0eNAGhJHq2SeQOn+kdEMevf5+95bJ98ECiYAJojOs06IH5ksBFjYy1wLWG9S3szwN angel@elitebook'>>/home/pesol/.ssh/authorized_keys
# --------------------------------------------------------------
chmod 600 /home/pesol/.ssh/authorized_keys
chown pesol:pesol /home/pesol/.ssh -R
## ------------------------------

# cambiar puerto ssh a uno aleatorio no permitir login root
echo "Port $ssh_port">>/etc/ssh/sshd_config
echo 'PermitRootLogin no'>>/etc/ssh/sshd_config

