#!/bin/bash

# ansible_pre_requisites.sh --odoo_passwd=xxx --pesol_passwd=xxx --ssh_port=xxx 

while [ $# -gt 0 ]; do
  case "$1" in
    --container_id=*)
      container_id="${1#*=}"
      ;;
    *)
      printf "***************************\n"
      printf "* Error: Invalid argument.*\n"
      printf "***************************\n"
      exit 1
  esac
  shift
done

sed '$d' /etc/netplan/10-lxc.yaml
sed '$d' /etc/netplan/10-lxc.yaml

echo "
      dhcp4: no
      addresses:
        - 10.0.3.$container_id/24
      gateway4: 10.0.3.1
      nameservers:
        addresses:
          - 8.8.8.8
          - 8.8.4.4

">>/etc/netplan/10-lxc.yaml
